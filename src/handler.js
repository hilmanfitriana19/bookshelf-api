// handler.js : file berisi tentang modul penanganan terhadap request dari user
const { nanoid } = require('nanoid')
const books = require('./books')

const addBookHandler = (request, h) => {
    const { name, year, author, summary, publisher, pageCount, readPage, reading } = request.payload
    // // checking all propery in request body
    let missingAttribute = '' // menampung nama properi yang tidak diinputkan oleh user sesuai dengan body response yang seharusnya
    if (name === undefined) {
        missingAttribute = 'nama'
    }
    if (!missingAttribute && year === undefined) {
        missingAttribute = 'tahun'
    }
    if (!missingAttribute && author === undefined) {
        missingAttribute = 'pengarang'
    }
    if (!missingAttribute && author === undefined) {
        missingAttribute = 'pengarang'
    }
    if (!missingAttribute && summary === undefined) {
        missingAttribute = 'ringkasan'
    }
    if (!missingAttribute && publisher === undefined) {
        missingAttribute = 'penerbit'
    }
    if (!missingAttribute && pageCount === undefined) {
        missingAttribute = 'pageCount'
    }
    if (!missingAttribute && readPage === undefined) {
        missingAttribute = 'readPage'
    }
    if (!missingAttribute && reading === undefined) {
        missingAttribute = 'reading'
    }
    // there one property not available in the request body
    if (missingAttribute) {
        return h.response({
            status: 'fail',
            message: `Gagal menambahkan buku. Mohon isi ${missingAttribute} buku`
        }).code(400)
    }
    // properti readPage lebih besar dari nilai properti pageCount
    if (readPage > pageCount) {
        return h.response({
            status: 'fail',
            message: 'Gagal menambahkan buku. readPage tidak boleh lebih besar dari pageCount'
        }).code(400)
    }

    // persiapan untuk menambahkan data ke array
    const id = nanoid(16)
    const finished = (pageCount === readPage)
    const insertedAt = new Date().toISOString()
    const updatedAt = insertedAt

    const newBook = {
        name, year, author, summary, publisher, pageCount, readPage, reading, id, finished, insertedAt, updatedAt
    }

    const initialBookLength = books.length
    // // push method return new length of array
    if (books.push(newBook) > initialBookLength) {
        const response = h.response({
            status: 'success',
            message: 'Buku berhasil ditambahkan',
            data: {
                bookId: id
            }
        })
        response.code(201)
        return response
    }
    // generic error
    return h.response({
        status: 'fail',
        message: 'Buku gagal ditambahkan'
    }).code(500)
}

const getAllBooksHandler = (request) => {
    // copy array of books
    let filteredData = [...books]
    const { reading, finished, name } = request.query
    // filter by query
    if (reading !== undefined) {
        filteredData = (parseInt(reading)) ? filteredData.filter(book => book.reading) : filteredData.filter(book => !book.reading)
    }

    if (finished !== undefined) {
        filteredData = (parseInt(finished)) ? filteredData.filter(book => book.finished) : filteredData.filter(book => !book.finished)
    }

    if (name !== undefined) {
        filteredData = filteredData.filter(book => book.name.toLowerCase().includes(name.toLowerCase()))
    }
    // get only specific properti
    filteredData = filteredData.map(({ id, name, publisher }) => ({ id, name, publisher }))
    return {
        status: 'success',
        data: {
            books: filteredData
        }
    }
}

const getBooksByIdHandler = (request, h) => {
    const { bookId } = request.params

    const book = books.find(book => book.id === bookId)

    if (book !== undefined) {
        return {
            status: 'success',
            data: {
                book
            }
        }
    }
    return h.response({
        status: 'fail',
        message: 'Buku tidak ditemukan'
    }).code(404)
}

const editBooksByIdHandler = (request, h) => {
    const { bookId } = request.params

    const index = books.findIndex(book => book.id === bookId)

    if (index === -1) {
        return h.response({
            status: 'fail',
            message: 'Gagal memperbarui buku. Id tidak ditemukan'
        }).code(404)
    }

    const { name, year, author, summary, publisher, pageCount, readPage, reading } = request.payload

    let missingAttribute = ''
    if (name === undefined) {
        missingAttribute = 'nama'
    }
    if (!missingAttribute && year === undefined) {
        missingAttribute = 'tahun'
    }
    if (!missingAttribute && author === undefined) {
        missingAttribute = 'pengarang'
    }
    if (!missingAttribute && author === undefined) {
        missingAttribute = 'pengarang'
    }
    if (!missingAttribute && summary === undefined) {
        missingAttribute = 'ringkasan'
    }
    if (!missingAttribute && publisher === undefined) {
        missingAttribute = 'penerbit'
    }
    if (!missingAttribute && pageCount === undefined) {
        missingAttribute = 'pageCount'
    }
    if (!missingAttribute && readPage === undefined) {
        missingAttribute = 'readPage'
    }
    if (!missingAttribute && reading === undefined) {
        missingAttribute = 'reading'
    }
    // there one property not available in the request body
    if (missingAttribute) {
        return h.response({
            status: 'fail',
            message: `Gagal memperbarui buku. Mohon isi ${missingAttribute} buku`
        }).code(400)
    }
    // properti readPage lebih besar dari nilai properti pageCount
    if (readPage > pageCount) {
        return h.response({
            status: 'fail',
            message: 'Gagal memperbarui buku. readPage tidak boleh lebih besar dari pageCount'
        }).code(400)
    }

    const updatedAt = new Date().toISOString()
    const finished = (pageCount === readPage)
    books[index] = { ...books[index], name, year, author, summary, publisher, pageCount, readPage, reading, finished, updatedAt }

    return h.response({
        status: 'success',
        message: 'Buku berhasil diperbarui'
    }).code(200)
}

const deleteBooksByIdHandler = (request, h) => {
    const { bookId } = request.params

    const index = books.findIndex(book => book.id === bookId)

    if (index === -1) {
        return h.response({
            status: 'fail',
            message: 'Buku gagal dihapus. Id tidak ditemukan'
        }).code(404)
    }

    books.splice(index, 1)
    return h.response({
        status: 'success',
        message: 'Buku berhasil dihapus'
    }).code(200)
}

module.exports = { addBookHandler, getAllBooksHandler, getBooksByIdHandler, editBooksByIdHandler, deleteBooksByIdHandler }
