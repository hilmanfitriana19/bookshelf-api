// modul untuk menampung konfigurasi server
const Hapi = require('@hapi/hapi')
const routes = require('./routes')
const dotenv = require('dotenv')
dotenv.config()

const init = async () => {
    const server = Hapi.server({
        port: process.env.port,
        host: process.env.NODE_ENV !== 'production' ? 'localhost' : '0.0.0.0',
        routes: {
            cors: {
                origin: ['*']
            }
        }
    })

    server.route(routes)

    await server.start()
    console.log(`Server berjalan pada ${server.info.uri}`)
}

init()
