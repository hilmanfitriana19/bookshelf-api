// menampung data buku

const books = [
    // example
    // {
    //     id: 'Qbax5Oy7L8WKf74l',
    //     name: 'Buku A Dicoding ',
    //     year: 2010,
    //     author: 'John Doe',
    //     summary: 'Lorem ipsum dolor sit amet',
    //     publisher: 'Dicoding Indonesia',
    //     pageCount: 100,
    //     readPage: 25,
    //     finished: false,
    //     reading: false,
    //     insertedAt: '2021-03-04T09:11:44.598Z',
    //     updatedAt: '2021-03-04T09:11:44.598Z'
    // },
    // {
    //     id: 'Abax5Oy7L8WKf74l',
    //     name: 'Buku A Dicoding ',
    //     year: 2010,
    //     author: 'John Doe',
    //     summary: 'Lorem ipsum dolor sit amet',
    //     publisher: 'Dicoding Indonesia',
    //     pageCount: 100,
    //     readPage: 25,
    //     finished: true,
    //     reading: false,
    //     insertedAt: '2021-03-04T09:11:44.598Z',
    //     updatedAt: '2021-03-04T09:11:44.598Z'
    // },
    // {
    //     id: 'Bbax5Oy7L8WKf74l',
    //     name: 'Buku A Dicoding ',
    //     year: 2010,
    //     author: 'John Doe',
    //     summary: 'Lorem ipsum dolor sit amet',
    //     publisher: 'Dicoding Indonesia',
    //     pageCount: 100,
    //     readPage: 25,
    //     finished: false,
    //     reading: true,
    //     insertedAt: '2021-03-04T09:11:44.598Z',
    //     updatedAt: '2021-03-04T09:11:44.598Z'
    // },
    // {
    //     id: 'Cbax5Oy7L8WKf74l',
    //     name: 'Buku A Dig ',
    //     year: 2010,
    //     author: 'John Doe',
    //     summary: 'Lorem ipsum dolor sit amet',
    //     publisher: 'Dicoding Indonesia',
    //     pageCount: 100,
    //     readPage: 25,
    //     finished: true,
    //     reading: true,
    //     insertedAt: '2021-03-04T09:11:44.598Z',
    //     updatedAt: '2021-03-04T09:11:44.598Z'
    // },
    // {
    //     id: 'Dbax5Oy7L8WKf7al',
    //     name: 'Buku A Dicding ',
    //     year: 2010,
    //     author: 'John Doe',
    //     summary: 'Lorem ipsum dolor sit amet',
    //     publisher: 'Dicoding Indonesia',
    //     pageCount: 100,
    //     readPage: 25,
    //     finished: false,
    //     reading: false,
    //     insertedAt: '2021-03-04T09:11:44.598Z',
    //     updatedAt: '2021-03-04T09:11:44.598Z'
    // }
]

module.exports = books
