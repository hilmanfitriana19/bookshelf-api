
# Bookshelf API

Aplikasi backend sederhana yang menyediakan API untuk menambah, mengubah, menampilkan dan menghapus data buku. Aplikasi dibangun menggunakan Node JS dengan hapi library.
Data disimpan dalam bentuk array. Inisiasi data array disimpan dalam file books.js.
Aplikasi dibuat untuk memenuhi submission Dicoding untuk kelas [Belajar Membuat Aplikasi Back-End untuk Pemula](https://www.dicoding.com/academies/261)

## Technology

- Node JS 16.13.1
- npm 8.1.2

## Getting Started
``` sh
#  Clone this repo to your local machine using
git clone https://gitlab.com/hilmanfitriana19/bookshelf-api.git

#  Install dependencies
npm install

#  Create .env file with copy from .env.sample
cp .env.sample .env

# Configure in .env file

#  Run App
npm run start-dev
```
## API Endpoint

## API Reference

#### Get all books
Get all data from book. Optional request can be specified with query based on reading status, finished status and specified with name.
```http
GET /books
```
#### Query Value
| Query     | Value    | Description                       |
| :-------- | :------- | :-------------------------------- |
| `reading` |  `0` | `false` |
| `reading` |  `1` | `true` |
| `finished` |  `0` | `false` |
| `finished` |  `1` | `true` |
| `name` |  `string` | search data by name containing the keyword `(case insensitive)` |


#### Get book 
Get specific data from book by id
```http
GET /books/${id}
```

#### Add book
Create book data to the system. The API will return the id if the process success.
```http
POST /books/
```
#### Parameter
| Key     |  Description                       |
| :-------- | :-------------------------------- |
| `name` |  name of the book |
| `year` |   year of the book publisher |
| `author` | author of the book |
| `summary` | summary of the book |
| `publisher` | publisher of the book |
| `pageCount` | number indicates how many page in the book |
| `readPage` | number indicates has read up to a certain page |
| `reading` | status about book being read or not |

#### PUT book 
Update book data with specific id. The request body has same structure with the POST method.
```http
PUT /books/${id}
```

#### DELETE book 
Delete book data with specific id.
```http
DELETE /books/${id}
```
